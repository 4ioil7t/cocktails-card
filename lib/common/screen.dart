import 'package:flutter/widgets.dart';

typedef ScreenBuilder<T extends IScreen> = T Function();

abstract class IScreen {}

abstract class StatelessScreen extends StatelessWidget implements IScreen {
  const StatelessScreen({Key? key}) : super(key: key);
}

abstract class StatefulScreen extends StatefulWidget implements IScreen {
  const StatefulScreen({Key? key}) : super(key: key);
}

abstract class ScreenArguments {}
