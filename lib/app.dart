import 'package:cocktails/screens/cocktails/bloc/cocktails_bloc.dart';
import 'package:cocktails/screens/cocktails/cubit/cocktail_cubit.dart';
import 'package:cocktails/screens/screens.barrel.dart';
import 'package:cocktails_repository/cocktails_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class App extends StatelessWidget {
  const App({Key? key, required CocktailsRepository cocktailsRepository})
      : _cocktailsRepository = cocktailsRepository,
        super(key: key);

  final CocktailsRepository _cocktailsRepository;

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
        providers: [
          RepositoryProvider.value(value: _cocktailsRepository),
        ],
        child: MultiBlocProvider(
          providers: [
            BlocProvider<CocktailsBloc>(
              create: (context) =>
                  CocktailsBloc(cocktailsRepository: _cocktailsRepository)
                    ..add(CocktailsLoaded()),
            ),
            BlocProvider<CocktailCubit>(
              create: (context) => CocktailCubit(),
            ),
          ],
          child: _AppView(),
        ));
  }
}

class _AppView extends StatefulWidget {
  @override
  _AppViewState createState() => _AppViewState();
}

class _AppViewState extends State<_AppView> {
  final _navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        backgroundColor: Colors.white,
        colorScheme: const ColorScheme(
          brightness: Brightness.light,
          primary: Colors.white,
          onPrimary: Colors.white,
          secondary: Color.fromRGBO(255, 87, 34, 1),
          onSecondary: Color.fromRGBO(255, 87, 34, 1),
          error: Colors.redAccent,
          onError: Colors.redAccent,
          background: Colors.white,
          onBackground: Colors.white,
          surface: Colors.white,
          onSurface: Colors.white,
        ),
        textTheme: const TextTheme(
          headline1: TextStyle(
            fontSize: 38,
            fontWeight: FontWeight.w300,
            color: Colors.black,
          ),
          headline2: TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.normal,
            color: Colors.black,
          ),
          headline3: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w500,
            color: Colors.black,
          ),
        ),
        scaffoldBackgroundColor: Colors.white,
        appBarTheme: const AppBarTheme(
          titleTextStyle: TextStyle(color: Colors.white),
          backgroundColor: Color.fromRGBO(255, 87, 34, 1),
        ),
      ),
      home: const SplashScreen(),
    );
  }
}
