import 'package:cocktails/app.dart';
import 'package:cocktails_repository/cocktails_repository.dart';
import 'package:flutter/material.dart';

void main() {
  CocktailsRepository cocktailsRepository = CocktailsRepository();
  runApp(
    App(
      cocktailsRepository: cocktailsRepository,
    ),
  );
}
