import 'package:bloc/bloc.dart';
import 'package:cocktails_repository/cocktails_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'cocktails_event.dart';
part 'cocktails_state.dart';

class CocktailsBloc extends Bloc<CocktailsEvent, CocktailsState> {
  CocktailsBloc({required CocktailsRepository cocktailsRepository})
      : _cocktailsRepository = cocktailsRepository,
        super(const CocktailsState()) {
    on<CocktailsLoaded>(_onCocktailsLoaded);
    on<CocktailSelected>((event, emit) {
      emit(CocktailsState(
          cocktailsList: state.cocktailsList,
          currentCocktail: event.currentCocktail));
    });
  }

  final CocktailsRepository _cocktailsRepository;

  Future<void> _onCocktailsLoaded(
      CocktailsLoaded event, Emitter<CocktailsState> emit) async {
    emit(state.copyWith(status: CocktailStatus.loading));
    try {
      final List<Cocktail> cocktails =
          await _cocktailsRepository.getCocktails();

      debugPrint(cocktails.toString());

      emit(state.copyWith(
          status: CocktailStatus.success, cocktailsList: cocktails));
    } catch (error) {
      debugPrint(error.toString());
      emit(state.copyWith(
          status: CocktailStatus.error, error: error.toString()));
    }
  }
}
