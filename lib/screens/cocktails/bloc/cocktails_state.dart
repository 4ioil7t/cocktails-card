part of 'cocktails_bloc.dart';

enum CocktailStatus { initial, success, error, loading }

extension CocktailsStatusGetter on CocktailStatus {
  bool get isInitial => this == CocktailStatus.initial;
  bool get isSuccess => this == CocktailStatus.success;
  bool get isError => this == CocktailStatus.error;
  bool get isLoading => this == CocktailStatus.loading;
}

class CocktailsState extends Equatable {
  const CocktailsState({
    this.currentCocktail,
    this.cocktailsList,
    this.error,
    this.status = CocktailStatus.initial,
  });

  final CocktailStatus status;
  final List<Cocktail>? cocktailsList;
  final Cocktail? currentCocktail;
  final String? error;

  CocktailsState copyWith({
    List<Cocktail>? cocktailsList,
    Cocktail? currentCocktail,
    CocktailStatus? status,
    String? error,
  }) {
    return CocktailsState(
      cocktailsList: cocktailsList ?? this.cocktailsList,
      currentCocktail: currentCocktail ?? this.currentCocktail,
      status: status ?? this.status,
      error: error ?? this.error,
    );
  }

  @override
  List<Object?> get props => [cocktailsList, currentCocktail, status, error];
}
