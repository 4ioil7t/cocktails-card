part of 'cocktails_bloc.dart';

@immutable
abstract class CocktailsEvent extends Equatable {
  const CocktailsEvent();

  @override
  List<Object> get props => [];
}

class CocktailsLoaded extends CocktailsEvent {}

class CocktailSelected extends CocktailsEvent {
  CocktailSelected({required this.currentCocktail});
  final Cocktail currentCocktail;
}
