import 'package:bloc/bloc.dart';
import 'package:cocktails_repository/cocktails_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'cocktail_state.dart';

class CocktailCubit extends Cubit<CocktailState> {
  CocktailCubit() : super(const CocktailState.empty());

  void selectPlacemark(Cocktail cocktail) {
    emit(state.copyWith(cocktail: cocktail));
  }

  void unselectPlacemark() {
    emit(const CocktailState.empty());
  }
}
