part of 'cocktail_cubit.dart';

enum CocktailStatus { selected, unselected }

class CocktailState extends Equatable {
  const CocktailState({required this.cocktail});
  const CocktailState.empty() : this.cocktail = null;

  final Cocktail? cocktail;

  bool get hasSelectedPlacemark => cocktail != null;

  CocktailState copyWith({
    Cocktail? cocktail,
  }) {
    return CocktailState(cocktail: cocktail ?? this.cocktail);
  }

  @override
  List<Object?> get props => [cocktail];
}
