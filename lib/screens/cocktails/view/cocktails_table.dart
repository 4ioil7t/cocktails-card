import 'package:cocktails/common/scaffold.dart';
import 'package:cocktails/common/screen.dart';
import 'package:cocktails/screens/cocktails/cocktails.barrel.dart';
import 'package:cocktails/screens/cocktails/cubit/cocktail_cubit.dart';
import 'package:cocktails_repository/cocktails_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CocktailTable extends StatefulScreen {
  const CocktailTable({Key? key, this.title, this.cocktailsList})
      : super(key: key);
  final String? title;
  final List<Cocktail>? cocktailsList;

  static Route<void> route() {
    return MaterialPageRoute<void>(builder: (context) => CocktailTable());
  }

  @override
  _CocktailTableState createState() => _CocktailTableState();
}

class _CocktailTableState extends State<CocktailTable> {
  bool _isTablet = false;
  late Cocktail currCocktail;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CocktailCubit, CocktailState>(
      buildWhen: (previous, current) => previous != current,
      builder: (context, state) {
        return MainScaffold(body: OrientationBuilder(
          builder: (context, orientation) {
            if (MediaQuery.of(context).size.width > 600) {
              _isTablet = true;
            } else {
              _isTablet = false;
            }
            return Flexible(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    child: ListView.separated(
                      separatorBuilder: (context, position) => const Padding(
                        padding: EdgeInsets.only(bottom: 20),
                      ),
                      itemCount: widget.cocktailsList!.length,
                      itemBuilder: (context, index) => GestureDetector(
                        onTap: () {
                          context
                              .read<CocktailCubit>()
                              .selectPlacemark(widget.cocktailsList![index]);

                          if (!_isTablet) {
                            Route myRoute = MaterialPageRoute(
                              builder: (context) => Scaffold(
                                appBar: AppBar(
                                  title: Text('Рецепт коктейля: ' +
                                      widget.cocktailsList![index].name),
                                ),
                                body: SafeArea(
                                  minimum:
                                      const EdgeInsets.fromLTRB(15, 20, 15, 0),
                                  child: CocktailRecipeScreen(),
                                ),
                              ),
                            );
                            Navigator.push(context, myRoute);
                          }
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Expanded(
                              child: Container(
                                padding: const EdgeInsets.all(12),
                                child: Text(
                                  widget.cocktailsList![index].name,
                                  style: const TextStyle(
                                    fontSize: 22,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.black,
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  color:
                                      Theme.of(context).scaffoldBackgroundColor,
                                ),
                              ),
                            ),
                            const Icon(Icons.arrow_right)
                          ],
                        ),
                      ),
                    ),
                  ),
                  _isTablet
                      ? Expanded(
                          child: SafeArea(
                            minimum: const EdgeInsets.symmetric(horizontal: 10),
                            child: CocktailRecipeScreen(),
                          ),
                        )
                      : Container(),
                ],
              ),
            );
          },
        ));
      },
    );
  }
}
