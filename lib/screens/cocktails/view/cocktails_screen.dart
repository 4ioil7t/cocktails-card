import 'package:cocktails/common/screen.dart';
import 'package:cocktails/screens/cocktails/bloc/cocktails_bloc.dart';
import 'package:cocktails/screens/screens.barrel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CocktailCardScreen extends StatelessScreen {
  const CocktailCardScreen({Key? key, this.title}) : super(key: key);
  final String? title;

  static Route<void> route() {
    return MaterialPageRoute<void>(
        builder: (context) => const CocktailCardScreen());
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CocktailsBloc, CocktailsState>(
      buildWhen: (previous, current) => previous != current,
      builder: (context, state) {
        switch (state.status) {
          case CocktailStatus.success:
            return CocktailTable(
              cocktailsList: state.cocktailsList,
            );

          case CocktailStatus.loading:
            return const LoadingScreen();

          case CocktailStatus.error:
            return const Center(child: Icon(Icons.cancel_outlined));

          default:
            return Center(
              child: Container(color: Colors.white),
            );
        }
      },
    );
  }
}
