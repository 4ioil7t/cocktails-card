import 'package:cocktails/screens/cocktails/cubit/cocktail_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CocktailRecipeScreen extends StatelessWidget {
  CocktailRecipeScreen({Key? key}) : super(key: key);

  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CocktailCubit, CocktailState>(
      buildWhen: (previous, current) => previous != current,
      builder: (context, state) {
        if (state.cocktail == null) {
          return Container();
        } else {
          return SingleChildScrollView(
            controller: _scrollController,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Text(
                    state.cocktail!.name,
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(bottom: 25),
                  child: Text(
                    'Рецепт приготовления',
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 15),
                  child: Text(
                    'Ингредиенты:',
                    style: Theme.of(context).textTheme.headline3,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 30),
                  child: ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    separatorBuilder: (context, position) => const Padding(
                      padding: EdgeInsets.only(bottom: 10),
                    ),
                    itemCount: state.cocktail!.ingredients.length,
                    itemBuilder: (context, index) => Row(
                      children: [
                        Text(
                          ("${index + 1}. "),
                        ),
                        Text(
                          (state.cocktail!.ingredients.keys.elementAt(index) +
                              ' '),
                        ),
                        Text(
                          state.cocktail!.ingredients.values.elementAt(index),
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Text(
                    state.cocktail!.description,
                    style: const TextStyle(fontSize: 16),
                  ),
                ),
                Container(
                  alignment: Alignment.topCenter,
                  child: Image.asset(
                    (state.cocktail!.photo ??
                        '/assets/logo_fruit_cocktail.png'),
                    width: 200,
                    height: 200,
                  ),
                ),
              ],
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
            ),
          );
        }
      },
    );
  }
}
