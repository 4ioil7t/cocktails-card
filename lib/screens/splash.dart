import 'dart:async';
import 'package:cocktails/screens/screens.barrel.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return SplashState();
  }
}

class SplashState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/logo_fruit_cocktail.png', height: 200),
            const Padding(padding: EdgeInsets.only(top: 20.0)),
            Text(
              "Cocktail Card",
              style: Theme.of(context).textTheme.headline1,
            ),
            const Padding(padding: EdgeInsets.only(top: 20.0)),
            CircularProgressIndicator(
              backgroundColor: Theme.of(context).colorScheme.secondary,
              strokeWidth: 1,
            )
          ],
        ),
      ),
    );
  }

  route() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => CocktailCardScreen()));
  }

  startTime() async {
    var duration = new Duration(seconds: 3);
    return new Timer(duration, route);
  }
}
