import 'dart:async';
import 'dart:developer';
import 'models/models.dart';

class CocktailsRepository {
  Future<List<Cocktail>> getCocktails() async {
    List<Cocktail> cocktails = <Cocktail>[];
    try {
      await Future.delayed(Duration(seconds: 2), () {
        // Do something
      });
      cocktails = [
        Cocktail(
          name: 'Маргарита',
          ingredients: {
            'Текила': '50 мл',
            'Трипл': '25 мл',
            'Сахарный сироп': '10 мл',
            'Лаймовый сок': '30 мл',
            'Лайм': '10 гр',
            'Соль': '2 гр',
            'Лед в кубиках': '200 гр',
          },
          description:
              'Сделай на бокале для маргариты соленую окаемку. Налей в шейкер лаймовый сок 30 мл, сахарный сироп 10 мл, ликер трипл сек 25 мл и серебряную текилу 50 мл. Наполни шейкер кубиками льда и взбей. Перелей через стрейнер в охлажденный бокал для маргариты. Укрась кружком лайма.',
          photo: 'assets/margarita.jpg',
        ),
        Cocktail(
          name: 'Дайкири',
          ingredients: {
            'Белый ром': '60 мл',
            'Сахарный сироп': '15 мл',
            'Лаймовый сок': '30 мл',
            'Лед в кубиках': '200 гр',
          },
          description:
              'Налей в шейкер лаймовый сок 30 мл, сахарный сироп 15 мл и белый ром 60 мл. Наполни шейкер кубиками льда и взбей. Перелей через стрейнер в охлажденное шампанское блюдце.',
          photo: 'assets/daikiri.jpg',
        ),
        Cocktail(
          name: 'Зеленая фея',
          ingredients: {
            'Серебряная Текила Sierra': '15 мл',
            'Абсент': '15 мл',
            'Водка Finlandia': '15 мл',
            'Белый ром': '15 мл',
            'Ликер Блю Кюрасао': '10 мл',
            'Дынный ликер': '30 мл',
            'Лимонный сок': '30 мл',
            'Энергетик': '100 мл',
            'Коктейльная вишня красная': '10 гр',
            'Лимонная цедра': '1 шт',
            'Лед в кубиках': '150 гр',
          },
          description:
              'Наполни слинг кубиками льда доверху. Налей лимонный сок 30 мл, ликер блю кюрасао 10 мл, дынный ликер 30 мл, белый ром 15 мл, водку 15 мл, серебряную текилу 15 мл и абсент 15 мл. Долей энергетик доверху и аккуратно размешай коктейльной ложкой. Укрась двумя коктейльными вишнями на шпажке и лимонной цедрой.',
          photo: 'assets/green-fairy.png',
        ),
        Cocktail(
          name: 'Беллини',
          ingredients: {
            'Просекко': '100 мл',
            'Сахарный сироп': '10 мл',
            'Лимонный сок': '10 мл',
            'Персиковое пюре': '20 гр',
            'Персик': '15 гр',
            'Лед в кубиках': '150 гр',
          },
          description:
              'Положи в стакан для смешивания персиковое пюре 4 к. ложки. Добавь лимонный сок 10 мл, сахарный сироп 10 мл и просекко 50 мл. Наполни стакан кубиками льда и аккуратно размешай коктейльной ложкой. Перелей через стрейнер и ситечко в бокал флюте. Долей просекко доверху. Укрась долькой персика.',
          photo: 'assets/bellini.jpg',
        ),
      ];
    } catch (e) {
      throw e;
    }
    return cocktails;
  }
}
